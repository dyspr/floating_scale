var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var xCount = 7
var yCount = 301

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < yCount; i++) {
    for (var j = 0; j < xCount; j++) {
      push()
      translate(windowWidth * 0.5 + (j - Math.floor(xCount * 0.5)) * boardSize * 0.1, windowHeight * 0.5 + (i - Math.floor(yCount * 0.5)) * boardSize * 0.0075 * 0.333)
      if (j % 2 === 0) {
        fill(128 + 128 * cos(frameCount * 0.05 + i * 0.025))
        noStroke()
        ellipse(0, 0, boardSize * 0.1 + boardSize * 0.05 * sin(frameCount * 0.05 + i * 0.1))
      } else {
        fill(128 + 128 * sin(frameCount * 0.05 + i * 0.025))
        noStroke()
        ellipse(0, 0, boardSize * 0.1 + boardSize * 0.05 * cos(frameCount * 0.05 + i * 0.1 + Math.PI * 0.5))
      }
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
